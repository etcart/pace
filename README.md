# kr

## Init
```bash
# Clone this repo and then...
pip install -r requirements.txt
npm i
```

## Running Locally (will reload when .py changes )
```bash
# Run the flask application; stdout/stderr will show in console
npm run server
```

## Rebuild javascript on changes ( webpack watch )
```bash
npm run ww
```

## Start Zookeeper and Kafka
```bash
$KAFKA_INSTALL_DIRECTORY/bin/zookeeper-server-start.sh $KAFKA_INSTALL_DIRECTORY/config/zookeeper.properties
$KAFKA_INSTALL_DIRECTORY/bin/kafka-server-start.sh $KAFKA_INSTALL_DIRECTORY/config/server.properties
```

## Start ETL Kafka Consumers/Producers
```bash
npm run kafka-intake
npm run kafka-clean
npm run kafka-predict
```


## Start Tensorflow Server on ML host
```bash
source activate tensorflow_p36
start predictor
```


## Running Locally (will reload when .py changes )
```bash
# Run the flask application; stdout/stderr will show in console
npm run server
```

## Environment Variables
Many values are configured via environment variables which must be set.
The environment configuration used is as follows (******* represents redacted values)
```
AWS_ACCESS_KEY_ID=*******
AWS_SECRET_ACCESS_KEY=*******
ELASTIC_ENDPOINT=*******
ELASTIC_USERNAME=*******
ELASTIC_PASSWORD=*******
ELASTIC_PORT=*******
ZOOKEEPER_ENDPOINT=*******
LANDING_ZONE=/tmp/zone
PROCESSED_ZONE=/tmp/processed
PREDICTOR_HOST_NAME=*******
PREDICTOR_HOST=*******
PREDICTOR_PORT=*******
MODEL_PATH=kr/ml/predictor/model
HASH_DIM=3000
HASH_NNZ=6


KAFKA_HOSTS=localhost:*******
KAFKA_LANDING_ZONE=/var/data/dropzone
FS_WARNING_THRESHOLD=100000000
KAFKA_FAILURE_DIRECTORY=/var/data/failures

KAFKA_TOPIC_RAW=kafka_raw_intake
KAFKA_TOPIC_CLEAN=kafka_clean_intake

KAFKA_INTAKE_CHUNK_SIZE=1000

ELASTIC_INDEX_GENERAL=ms_general
KAFKA_TOPICS_COUNT_DIRECTORY=kr/kafka/kafkaPipeline/counts
KAFKA_INSTALL_DIRECTORY=/usr/local/kafka

DETECTION_THRESHOLD=0.7

KAFKA_LOG_RAW=kafka_log_intake
KAFKA_LOG_CLEAN=kafka_log_clean
KAFKA_LOG_PREDICTION=kafka_log_prediction
FIT_LOG_INDEX=tensorflow_log_fit
PREDICT_LOG_INDEX=tensorflow_log_predict

DATA_LAKE_PATH=/var/data/lake
```


## License
NO LICENSE GRANTED
