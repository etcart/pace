#!/usr/bin/env python

import shutil
import json
import sys
from os import environ as env
import os
from kafka import KafkaConsumer
from elasticsearch import Elasticsearch
from threading import Thread
import requests
from statistics import mean
import glob

#stats template for reference
#     results = {
#         'name': socket.gethostname(),
#         'totalRAM': totalRAM(),
#         'RAM': RAMUsePercent(),
#         'cpu': cpuUse(),
#     }

def returnError(e):
    print(e, file=sys.stderr)
    return {'state': 'ERROR'}
def fileSize(filename):
    if os.path.isfile(filename):
        return os.path.getsize(filename)
    return 0
def dirSize(dirPath):
    if os.path.islink(dirPath):
        return 0
    return sum([dirSize(name) if os.path.isdir(name) else fileSize(name) for name in glob.glob(dirPath+"/*")])

def dirCount(dirPath):
    fileNames = glob.iglob(dirPath + '/*')
    fileCount = 0
    for fileName in fileNames:
        if os.path.isdir(fileName) and not os.path.islink(fileName):
            fileCount += dirCount(fileName)
        else:
            fileCount += 1
    return fileCount

def percentDone():
    totalSize = 100
    return 100*dirCount(env['resultsDirectory'])/totalSize

def loadStatus(host):
    try:
        return json.loads(requests.get('http://{}:{}/state'.format(host, env['statQueryPort'])).content)
    except:
        return None

def system_status():
    hosts_per_line = 4
    hosts = env['workerIPs'].split(',')

    
    results = []
    for host in hosts:
        result = loadStatus(host)
        if result is None:
            continue
        results.append(result)

    final = {
        "head": {
            "done": percentDone(),
            "RAM": mean([machine['RAM'] for machine in results]),
            "cpu": mean([machine['cpu'] for machine in results]),
        },
        "workers": results[:hosts_per_line] if len(results)>hosts_per_line else results
    }
    return final


