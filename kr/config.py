import os
import time

CLIENT_VERSION = os.environ.get('CLIENT_VERSION', 'unknown')
VERSION = os.environ.get('VERSION', time.time())
STAGE = os.environ.get('STAGE', 'dev')
CDN = os.environ.get('CDN', '')
ELASTIC_USERNAME = os.environ.get('ELASTIC_USERNAME')
ELASTIC_PASSWORD = os.environ.get('ELASTIC_PASSWORD')

config = {
    'stage': STAGE,
    'cdn': CDN,
    'setup': False,
    'v': VERSION,
    'datasets': {
        'mshosts': {
            'id': 'mshosts',
            'name': 'Windows Hosts',
            'elastic_index': 'ms_general',
            # 'elastic_index': 'ms_train',
            'format': 'misc'
        }
    }
}
