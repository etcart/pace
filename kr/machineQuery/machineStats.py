#requires psutils

import psutil
import glob
import os
import socket
from waitress import serve
import socket
from os import environ as env
from flask import Flask, jsonify
def humanSize(byteSize):
    formats = [
        (1024 ** 5, 'P'),
        (1024 ** 4, 'T'),
        (1024 ** 3, 'G'),
        (1024 ** 2, 'M'),
        (1024 ** 1, 'K'),
        (1024 ** 0, 'B'),
    ]
    for factor, suffix in formats:
        if byteSize >= factor:
            break
    amount = str(round(byteSize / factor, 2))
    period = amount.index('.')
    if period > 1:
        amount = amount[:period]
    return amount + suffix

def totalRAM():
    return humanSize(psutil.virtual_memory()[0])

def RAMUsePercent(decimalForm=False):
    return psutil.virtual_memory()[2]

def cpuUse():
    return psutil.cpu_percent()
    
def fileSize(filename):
    if os.path.isfile(filename):
        return os.path.getsize(filename)
    return 0
def dirSize(dirPath):
    if os.path.islink(dirPath):
        return 0
    return sum([dirSize(name) if os.path.isdir(name) else fileSize(name) for name in glob.glob(dirPath+"/*")])

def dirCount(dirPath):
    fileNames = glob.iglob(dirPath + '/*')
    fileCount = 0
    for fileName in fileNames:
        if os.path.isdir(fileName) and not os.path.islink(fileName):
            fileCount += dirCount(fileName)
        else:
            fileCount += 1
    return fileCount

# def percentDone():
#     totalSize = 100
#     return 100*dirCount(env['resultsDirectory'])/totalSize


app = Flask(__name__)

@app.route('/state', methods=['GET'])
def state():
    results = {
        'name': socket.gethostname(),
        'totalRAM': totalRAM(),
        'RAM': RAMUsePercent(),
        'cpu': cpuUse(),
        # 'done': percentDone(),
    }
    return jsonify(results)


if __name__ == '__main__':

    port = env['statQueryPort']
    if socket.gethostname() == env['hostName']:
        from waitress import serve
        serve(app, host=env['hostIP'], port=port)
    else:
        app.run(host='0.0.0.0', port=port)
