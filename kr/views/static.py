from kr.config import config
from kr.responders import success, error
from urllib import request as http_req, parse
from flask import Flask, Blueprint, request, redirect, session, escape, send_from_directory, render_template, Response

static = Blueprint('static', __name__, url_prefix='/')


@static.route('/img/<path:path>')
def send_img(path):
    """ Controller method to respond to image files

    Args:
        path: Path component of URL

    Returns:
        Image File contents

    """
    return send_from_directory('./web/img', path)


@static.route('/js/<path:path>')
def send_js(path):
    """ Controller method to respond to js files

    Args:
        path: Path component of URL

    Returns:
        JS File contents

    """
    return send_from_directory('./web/js', path)


@static.route('/vendors/<path:path>')
def send_vendors(path):
    """ Controller method to respond to vendor package files

    Args:
        path: Path component of URL

    Returns:
        File contents

    """
    return send_from_directory('./web/vendors', path)


@static.route('/css/<path:path>')
def send_css(path):
    """ Controller method to respond to css files

    Args:
        path: Path component of URL

    Returns:
        CSS File contents

    """
    return send_from_directory('./web/css', path)


@static.route('/fonts/<path:path>')
def send_fonts(path):
    """ Controller method to respond to font files

    Args:
        path: Path component of URL

    Returns:
        Font File contents

    """
    return send_from_directory('./web/fonts', path)


