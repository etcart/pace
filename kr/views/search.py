from kr.config import config
from kr.responders import success, error
from urllib import request as http_req, parse
from flask import Flask, Blueprint, request, redirect, session, escape, send_from_directory, render_template, Response
from jsonschema import validate, ValidationError
from pydash import _
import uuid
import time
import os
import simplejson as json
import hashlib
import pyotp
import copy
import rollbar
import rollbar.contrib.flask
from os import environ as env
from flask import got_request_exception
from elasticsearch import Elasticsearch

search = Blueprint('search', __name__, url_prefix='/search')


@search.route('/', methods=['GET'])
def page():
    """ Controller method to serve Explore page

    Returns:
        Explore page contents
    """
    dataset = request.args.get('ds')

    if not dataset:
        dataset = 'mshosts'

    context = copy.deepcopy(config)
    context['current_dataset'] = config['datasets'][dataset]

    return render_template('ltr/horizontal-menu-template/explore.html', **context)


@search.route('/query', methods=['GET', 'POST'])
def query():
    """ Controller method to respond to ajax search requests

    Returns:
        Dataset records in JSON format

    """
    dataset = request.args.get('ds') if request.args.get('ds') else 'mshosts'
    index = config['datasets'][dataset]['elastic_index']

    query = request.args.get('q')
    threshold = float(request.args.get('threshold')) if request.args.get('threshold') else 0

    req = {
        "size": 1000,
        "query": {
            "range": {
                "HasDetections": {
                    "gte": threshold,
                    "boost": 2.0
                }
            }
        }
    }

    if threshold > 0:
        req['size'] = 1000

    if query:
        query = '*' + query + '*'
        req = {
            "size": 1000,
            "query": {
                "function_score": {
                    "query": {
                        "bool": {
                            "filter": [
                                {
                                    "query_string": {
                                        "query": query,
                                        "fields": [
                                            "MachineIdentifier",
                                            "Census_ActivationChannel",
                                            "Census_ChassisTypeName",
                                            "Census_DeviceFamily",
                                            "Census_FlightRing",
                                            "Census_OSEdition",
                                            "Platform.keyword",
                                            "Census_MDC2FormFactor",
                                            "OsVer",
                                            "OsBuild.keyword",
                                            "OsSuite.keyword",
                                            "OsPlatformSubRelease",
                                            "SkuEdition"
                                        ]
                                    }
                                },
                                {
                                    "range": {
                                        "HasDetections": {"gte": threshold}
                                    }
                                }
                            ]
                        }
                    },
                    "field_value_factor": {
                        "field": "HasDetections"
                    }
                }
            }
        }

    es = Elasticsearch('http://{}:{}'.format(env['ELASTIC_ENDPOINT'], env['ELASTIC_PORT']), http_auth=(env['ELASTIC_USERNAME'], env['ELASTIC_PASSWORD']), use_ssl=False)
    res = es.search(index=index, body=req)
    results = []

    for item in res['hits']['hits']:
        item = item['_source']

        name = "%s %s %s %s" % (item['Platform'].capitalize(), item['Census_OSVersion'], item['Census_OSEdition'], item['Census_MDC2FormFactor'])

        row = {
            'MachineIdentifier': name,
            'OsVer': item['OsVer'],
            'OsBuild': item['OsBuild'],
            'OsPlatformSubRelease': item['OsPlatformSubRelease'],
            'SkuEdition': item['SkuEdition'],
            'Vulnerability': str(int(item['HasDetections'] * 100))
        }
        results.append(row)

    return success(results)
