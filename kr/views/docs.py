# from kr.dynamo import Dao
# from kr.mailer import Mailer
# from kr.gatekeeper import GateKeeper
# from kr.config import config
from kr.responders import success, error
from urllib import request as http_req, parse
from flask import Flask, Blueprint, request, redirect, session, escape, send_from_directory, render_template, Response
from jsonschema import validate, ValidationError
from pydash import _
import uuid
import time
import os
import simplejson as json
import hashlib
import pyotp
import copy
import rollbar
import rollbar.contrib.flask
from flask import got_request_exception

docs = Blueprint('docs', __name__, url_prefix='/docs')


@docs.route('/', methods=['GET'])
@docs.route('/<path:path>', methods=['GET'])
def page(path=None):
    """ Controller method to respond to vendor package files

    Args:
        path: Path component of URL

    Returns:
        File contents

    """
    if not path:
        path = 'index.html'
    return send_from_directory('./docs/_build/html', path)
