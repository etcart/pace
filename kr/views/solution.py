# from kr.dynamo import Dao
# from kr.mailer import Mailer
# from kr.gatekeeper import GateKeeper
# from kr.config import config
from kr.responders import success, error
from urllib import request as http_req, parse
from flask import Flask, Blueprint, request, redirect, session, escape, send_from_directory, render_template, Response
from jsonschema import validate, ValidationError
from pydash import _
import uuid
import time
import os
import simplejson as json
import hashlib
import pyotp
import copy
import rollbar
import rollbar.contrib.flask
from flask import got_request_exception

solution = Blueprint('solution', __name__, url_prefix='/solution')


@solution.route('/', methods=['GET'])
def solution_main():
    context = {}
    return render_template('docs.jinja', **context)


@solution.route('/<path:path>')
def send_solution_page(path):
    context = {}
    return render_template('docs/' + path + '.html', **context)
