from kr.config import config
from kr.responders import success, error
from urllib import request as http_req, parse
from flask import Flask, Blueprint, request, redirect, session, escape, send_from_directory, render_template, Response
from jsonschema import validate, ValidationError
from pydash import _
import uuid
import time
import os
import simplejson as json
import hashlib
import pyotp
import copy
import rollbar
import rollbar.contrib.flask
from os import environ as env
from flask import got_request_exception
from elasticsearch import Elasticsearch

visualize = Blueprint('visualize', __name__, url_prefix='/vis')


def getAggCounts(name, agg):
    children = []
    for bucket in agg['buckets']:

        item = {
            'name': bucket['key'],
            'label': name,
            'value': bucket['doc_count'],
        }

        try:
            if int(item['name']) == 0:
                item['name'] = 'No ' + name
            else:
                item['name'] = name + ': ' + item['name']
        except ValueError:
            pass

        for key in bucket:
            if isinstance(bucket[key], dict):
                item['children'] = getAggCounts(key, bucket[key])
        children.append(item)

    return children


def buildAggTree(fields, n):
    agg = {
        fields[n]: {
            "terms": {"field": fields[n] + ".keyword"}
        }
    }
    if n < len(fields) - 1:
        agg[fields[n]]['aggs'] = buildAggTree(fields, n + 1)
    return agg


@visualize.route('/home', methods=['GET'])
def main():
    context = copy.deepcopy(config)
    if 'setup' in session and session['setup']:
        context['setup'] = True
        session.pop('setup', None)

    return render_template('ltr/horizontal-menu-template/sunburst.html', **context)


@visualize.route('/kibana', methods=['GET'])
def page():
    """ Controller method to serve Dashboard page

    Returns:
        Dashboard page contents
    """

    return render_template('ltr/horizontal-menu-template/kibana.html')

@visualize.route('/maps', methods=['GET'])
def maps():
    return render_template('ltr/horizontal-menu-template/maps.html')

@visualize.route('/sunburst', methods=['GET'])
def sunburst():

    fields_str = request.args.get('fields')
    fields = fields_str.split(',')
    es = Elasticsearch('http://{}:{}'.format(env['ELASTIC_ENDPOINT'], env['ELASTIC_PORT']), http_auth=(env['ELASTIC_USERNAME'], env['ELASTIC_PASSWORD']), use_ssl=False)

    agg = {
        "aggs": buildAggTree(fields, 0)
    }
    res = es.search(index='ms_train', body=agg)

    results = {
        'name': 'Computers',
        # 'value': es.count(index='ms_train')['count'],
        'children': getAggCounts(fields[0], res['aggregations'][fields[0]])
    }

    return success(results)
