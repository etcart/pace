from flask import Response
import simplejson as json


def error(response='NOT_OKAY', status=400):
    return Response(json.dumps(response), mimetype='application/json', status=status)


def success(response='OKAY', status=200):
    return Response(json.dumps(response), mimetype='application/json', status=status)
