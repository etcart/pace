#!/usr/bin/env python

import requests
import uuid
import time
import os
import simplejson as json
import hashlib
import pyotp
import copy
import rollbar
import rollbar.contrib.flask
import socket

from kr.config import config
from kr.responders import success, error
from urllib import request as http_req, parse
from flask import Flask, Blueprint, request, redirect, session, escape, send_from_directory, render_template, Response
from jsonschema import validate, ValidationError
from pydash import _

from flask import got_request_exception
from elasticsearch import Elasticsearch

from kr.views.static import static
from kr.views.docs import docs
from kr.views.solution import solution
from kr.views.search import search
from kr.views.visualize import visualize
from kr.status import system_status


app = Flask(__name__, template_folder="./web/templates")
port = int(os.getenv("PORT")) if os.getenv("PORT") else 5000

app.register_blueprint(static)
app.register_blueprint(docs)
app.register_blueprint(solution)
app.register_blueprint(search)
app.register_blueprint(visualize)

STAGE = os.environ.get('STAGE', 'dev')
app.secret_key = os.environ.get('FLASK_SECRET_KEY')


@app.before_first_request
def init():
    pass
    # print('external ip ' + requests.get('http://checkip.amazonaws.com').text.rstrip())


@app.route('/', methods=['GET'])
def main():
    context = copy.deepcopy(config)
    return render_template('ltr/horizontal-menu-template/welcome.html', **context)


@app.route('/flowdash', methods=['GET'])
def flowdash():
    context = copy.deepcopy(config)
    context['system'] = system_status()
    return render_template('ltr/horizontal-menu-template/flowdash.html', **context)


@app.route('/flowdash/ajax', methods=['GET', 'POST'])
def flowdash_ajax():
    context = copy.deepcopy(config)
    result = system_status()
    return success(result)


if __name__ == '__main__':
    if socket.gethostname() == 'ip-172-30-0-200':
        from waitress import serve
        serve(app, host='172.30.0.200', port=9439)
    else:
        app.run(host='0.0.0.0', port=port)
