// const uuid = require('uuid/v4');
const moment = require('moment');
const _ = require('lodash');
const d3 = require('d3');
const swal = require('sweetalert2');
// const Sunburst = require('sunburst-chart');
// import Chartist from 'chartist';
// import CanvasJS from 'canvasjs';
// const Chartist = require('chartist');
require('../sass/kr.scss');

let state = {
    sunburst: true,
    sunburst_data: null,
    search_vuln: false,
    vuln_toggle: false
};

let search = (query, table) => {

    $('.search-pulse').addClass('pulsefade');
    // setTimeout(function() {
    //     $('.search-pulse').removeClass('pulse');
    // }, 1000);

    let url = '/search/query?q=' + query;
    if (state.search_vuln) {
        url += '&threshold=0.7';
    }

    $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
            var jsonObject = $.parseJSON(JSON.stringify(response));

            var result = jsonObject.map(function(item) {
                var result = [];
                result.push(item.MachineIdentifier);
                result.push(item.OsVer);
                result.push(item.OsBuild);
                result.push(item.OsPlatformSubRelease);
                result.push(item.SkuEdition);
                let color = 'black';
                if (item.Vulnerability >= 90) {
                    color = 'red';
                } else if (item.Vulnerability >= 70) {
                    color = 'orange';
                }
                result.push(`<span class="${color}-text">` + item.Vulnerability + '</span>');
                return result;
            });

            table.clear(); //clear table before loading new results
            table.rows.add(result); // add to DataTable instance
            table.draw(); // always redraw with new results
            $('.search-pulse').removeClass('pulsefade');
        },
        error: function(res) {
            console.error(res);
        }
    });
};

function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function() {
            callback.apply(context, args);
        }, ms || 0);
    };
}

function statBlock(stats){
    return `<div class="col s3">
        <div class="card">
            <div class="card-content">
                <!-- <img class="activator right logo" src="/img/zfs.png"> -->
                <i class="activator right logo material-icons">storage</i>
                <span class="card-title activator grey-text text-darken-4">${stats['name']}<br><span class="grey-text">Worker Node</span></span>
                <div class="stats-lander">
                    ${stats['RAM']} / ${stats['totalRAM']}<br>
                    ${stats['cpu']} <br>

                </div>

                <a class="activator etl-indicator btn-floating btn-flat halfway-fab right waves-effect waves-light white lighten-1"><i class="material-icons">info</i></a>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Worker Node<i class="material-icons right">close</i></span>
                <p>A machine responsible in part for the calculation of the ufs weather forecast</p>
            </div>
        </div>
    </div>`
   
}
let flow_update = function() {
    $.ajax({
        url: '/flowdash/ajax',
        type: 'POST',
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(system) {
            var html = "";
            var workers = system['workers']
            for(var i = 0; i<workers.length; i++){
                    html += statBlock(workers[i]);
            }
            console.log(workers);
            $('.stats').html(html);
            $('.headRAM').html(`${system['head']['RAM']}% RAM Usage`);
            $('.headCPU').html(`${system['head']['cpu']}% CPU Usage`);
            $('.headDone').html(`${system['head']['done']}% complete`);
            
            
            // var data = {
            //     labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
            //     series: [
            //       [5, 2, 4, 2, 0]
            //     ]
            //   };
            // var options = {
            //     width: '300px',
            //     height: '200px',
            //     lineSmooth: Chartist.Interpolation.cardinal({
            //         tension: 0.2
            //     })
            // }
              
            //   new Chartist.Bar('.ct-chart', data, options);
            // var chart = new CanvasJS.Chart("ct-chart", {
            //     animationEnabled: true,
                


            // $('.stats-elastic').addClass('pulse');
            // $('.stats-kafka').addClass('pulse');
            // $('.stats-tensorflow').addClass('pulse');
            // $('.stats-app').addClass('pulse');

            // setTimeout(function() { $('.stats-elastic').removeClass('pulse'); }, 500);
            // setTimeout(function() { $('.stats-kafka').removeClass('pulse'); }, 500);
            // setTimeout(function() { $('.stats-tensorflow').removeClass('pulse'); }, 500);
            // setTimeout(function() { $('.stats-app').removeClass('pulse'); }, 500);

        },
        error: function(res) {
            console.error(res);
        }
    });
};

$(document).ready(function() {

    $('.tabs').tabs();
    $('select').formSelect();
    $('.tooltipped').tooltip();
    $('.modal').modal();

    if ($('#main.flowdash').length) {

        flow_update();
        setInterval(flow_update, 3000);
    }

    if ($('#explore-search').length) {

        const searchTable = $('#explore-table').DataTable({
            responsive: true,
            scrollY: '50vh',
            scrollCollapse: true,
            paging: false,
            bFilter: false,
            language: {
                searchPlaceholder: 'Search',
                search: '',
            },
        });

        let params = getUrlVars();
        if (_.has(params, 'vuln')) {
            state.search_vuln = true;
            $('.vuln-switch input').prop('checked', true);
        }

        $('.vuln-switch').on('click', function() {
            console.log('clickevent');
            if (!state.vuln_toggle) {
                state.search_vuln = !state.search_vuln;
                search($('#explore-search').val(), searchTable);
                state.vuln_toggle = true;
            } else {
                state.vuln_toggle = false;
            }
        });

        $('#explore-search').keyup(delay(function() {
            search(this.value, searchTable);
        }, 500));

        search('', searchTable);
    }

    if ($('#dash').length) {

        let flow_opts = {
            lineWidth: 2,
            lineSpacerWidth: 15,
            lineColour: '#66bb6a',
            canvasElm: '.canvas'
        };

        $('.flowcard').SimpleFlow(flow_opts);
        // $('.flow-kafka').SimpleFlow(flow_opts);
        // $('.flow-elastic').SimpleFlow(flow_opts);
        // $('.flow-ml').SimpleFlow(flow_opts);
        // $('.flow-lake').SimpleFlow(flow_opts);
        // $('.flow-sas').SimpleFlow(flow_opts);
    }

    if ($('#dash').length) {
        update_dash();
    }

    $('#switch-sunburst').on('click', function() {
        state.sunburst = !state.sunburst;
        update_dash();
    });

    $('.fields select').on('change', function() {
        update_dash();
    });

    let table_options_logs = {
        responsive: true,
        scrollY: '50vh',
        scrollCollapse: true,
        paging: false,
        language: {
            searchPlaceholder: 'Search',
            search: '',
        },
        ajax: {
            url: '/search/explore?dataset=logs_test',
            type: 'GET'
        },
        columns: [{
                data: 'time',
                render: function(data) {
                    return moment(data * 1000).toISOString();
                }
            },
            {
                data: 'host'
            },
            {
                data: 'method'
            },
            {
                data: 'path'
            },
            {
                data: 'status'
            },
            {
                data: 'bytes'
            }
        ]
    };

    //     let table_options_mshosts = {
    //         responsive: true,
    //         scrollY: '50vh',
    //         scrollCollapse: true,
    //         paging: false,
    //         bFilter: false,
    //         language: {
    //             searchPlaceholder: 'Search',
    //             search: '',
    //         },
    //         ajax: {
    //             // url: '/search/explore?ds=mshosts&explore-search=' + $('#explore-search').val(),
    //             url: '/search/explore?ds=mshosts',
    //             type: 'GET'
    //         },
    //         columns: [{
    //                 data: 'MachineIdentifier',
    //             },
    //             {
    //                 data: 'OsVer'
    //             },
    //             {
    //                 data: 'OsBuild'
    //             },
    //             {
    //                 data: 'OsSuite'
    //             },
    //             {
    //                 data: 'OsPlatformSubRelease'
    //             },
    //             {
    //                 data: 'SkuEdition'
    //             }
    //         ]
    //     };

    //     let $thead = $('#explore-everything table').find('thead');
    //     if ($thead) {
    //         $thead.html(`<tr>
    //         <th>MachineIdentifier</th>
    //         <th>OsVer</th>
    //         <th>OsBuild</th>
    //         <th>OsSuite</th>
    //         <th>OsPlatformSubRelease</th>
    //         <th>SkuEdition</th>
    //     </tr>`);
    //     }


    //    $('#explore-everything table[data-dataset=mshosts]').DataTable(table_options_mshosts);

});

// let walkthrough_setup = function() {

//     $('.modal').modal({
//         'onOpenEnd': initCarouselModal,
//     });

//     // setTimeout(function () { $('.modal').modal('open'); }, 1800)


//     $('.btn-next').on('click', function() {
//         $('.intro-carousel').carousel('next');
//     });

//     $('.btn-prev').on('click', function() {
//         $('.intro-carousel').carousel('prev');
//     });

//     // Inti carousel when modal pops up

//     function initCarouselModal() {
//         $('.carousel.carousel-slider').carousel({
//             fullWidth: true,
//             indicators: true,
//             onCycleTo: function() {

//                 // When carousel is at it's first step disable prev button

//                 if ($('.carousel-item.active').index() == 1) {
//                     $('.btn-prev').addClass('disabled');

//                 }

//                 // When carousel is at 2nd or 3rd step
//                 else if ($('.carousel-item.active').index() > 1) {

//                     // activate button

//                     $('.btn-prev').removeClass('disabled');
//                     $('.btn-next').removeClass('disabled');

//                     // on 3rd step add and remove elements

//                     if ($('.carousel-item.active').index() == 3) {
//                         $('.btn-next').addClass('disabled');
//                     }
//                 }
//             }
//         });
//     }

//     $(document).on('click', '[data-action=walkthrough]', function() {
//         $('#walkthrough-nasa').find('.modal').modal('open');
//     });

//     $(document).on('click', '[data-href]', function() {
//         window.location.href = $(this).attr('data-href');
//     });
// };

// let show_counts = function() {

//     let $counts = $('.dataset-counts');

//     let color_map = {
//         'logs_test': 'cyan',
//         'wine': 'purple'
//     };

//     $.ajax({
//         url: '/search/counts',
//         type: 'GET',
//         cache: false,
//         dataType: 'json',
//         success: function(resp) {
//             $counts.html('');
//             _.each(resp, (v, k) => {
//                 let color = _.has(color_map, k) ? color_map[k] : 'grey';
//                 let count = v.count;
//                 if (count > 1000000) {
//                     count = (count / 1000000).toFixed(2) + 'M Records';
//                 } else {
//                     count = count + ' Records';
//                 }
//                 $counts.append(`<div class="col s12">
//          <div class="card animate fadeLeft" data-source="${v.id}" data-href="/explore?ds=${v.id}">
//             <div class="card-content ${color} white-text">
//                <p class="card-stats-title"><i class="material-icons">storage</i>${v.name}</p>
//                <h4 class="card-stats-number white-text">${count}</h4>
//                <p class="card-stats-compare">
//                   <i class="material-icons">keyboard_arrow_up</i> 100%
//                   <span class="${color} text text-lighten-5">from yesterday</span>
//                </p>
//             </div>
//             <div class="card-action ${color} darken-1">
//                <div id="clients-bar" class="center-align"></div>
//             </div>
//          </div>
//       </div>`);
//             });
//         },
//         error: function(err) {
//             console.log(err);
//         }
//     });
// };

let update_dash = () => {

    if ($('#mainvis').length) {

        $('#mainvis').html('');

        let fields = '';
        $('.fields select').each(function() {
            fields += $(this).val() + ',';
        });
        fields = fields.slice(0, -1);

        // let fields = 'Census_MDC2FormFactor,AVProductsInstalled,AVProductsEnabled';

        $.ajax({
            url: '/vis/sunburst?fields=' + fields,
            type: 'GET',
            cache: false,
            dataType: 'json',
            success: function(data) {

                // data = _.cloneDeepWith(data, v => v === '0' ? 'No' : undefined);
                // data = _.cloneDeepWith(data, v => v === '1' ? 'Yes' : undefined);
                console.log(data);


                let colors = ['#42a5f5', '#7e57c2', '#66bb6a', '#ffa726', '#4db6ac', '#81d4fa'];

                // const color = d3.scaleOrdinal(d3.schemeRdB);
                // const color = d3.scaleOrdinal(d3.schemePaired);
                const color = d3.scaleOrdinal(colors);

                let vis = null;

                if (state.sunburst) {
                    vis = Sunburst();
                } else {
                    vis = Icicle();
                }

                vis
                    .data(data)
                    .label('name')
                    .size('value') // TODO some scale to min size?
                    // .color((d, parent) => color(parent ? parent.data.name : null))
                    .color((d, parent) => color(parent ? parent.data.name : null))
                    .tooltipContent((d, node) => `${node.data.label}: <i>${node.value}</i>`)
                    .width(800)
                    .height(600)(document.getElementById('mainvis'));

                // sunburst
                //     .data(data)
                //     .label('name')
                //     .size('value') // TODO some scale to min size?
                //     // .color((d, parent) => color(parent ? parent.data.name : null))
                //     .color((d, parent) => color(parent ? parent.data.name : null))
                //     .tooltipContent((d, node) => `${node.data.label}: <i>${node.value}</i>`)
                //     .width(800)
                //     .height(600)(document.getElementById('mainvis'));

            },
            error: function() {

            }
        });
    }
};

// let show_predictions = function(data) {
//     let $table = $('tbody#prediction-results');
//     $table.html('');

//     _.each(data, (v) => {
//         $table.append(`<tr><td>${v.name}</td><td>${v.predicted}</td><td>${v.actual}</td></tr>`);
//     });
// };

function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}