import os
from kr.ml.predictor.initializePredictor import initializePredictor

if not os.path.isdir(os.environ['MODEL_PATH']):
    os.mkdir(os.environ['MODEL_PATH'])

model = initializePredictor()
model.serve(
    prodHost='172.30.9.200',
    prodHostName=os.environ['PREDICTOR_HOST_NAME'],
    port=os.environ['PREDICTOR_PORT']
)
