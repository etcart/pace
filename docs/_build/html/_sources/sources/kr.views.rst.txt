kr.views package
================

Submodules
----------

kr.views.analyze module
-----------------------

.. automodule:: kr.views.analyze
   :members:
   :undoc-members:
   :show-inheritance:

kr.views.docs module
--------------------

.. automodule:: kr.views.docs
   :members:
   :undoc-members:
   :show-inheritance:

kr.views.search module
----------------------

.. automodule:: kr.views.search
   :members:
   :undoc-members:
   :show-inheritance:

kr.views.solution module
------------------------

.. automodule:: kr.views.solution
   :members:
   :undoc-members:
   :show-inheritance:

kr.views.static module
----------------------

.. automodule:: kr.views.static
   :members:
   :undoc-members:
   :show-inheritance:

kr.views.visualize module
-------------------------

.. automodule:: kr.views.visualize
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: kr.views
   :members:
   :undoc-members:
   :show-inheritance:
