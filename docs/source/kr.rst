kr package
==========

Subpackages
-----------

.. toctree::

   kr.responders

Submodules
----------

kr.config module
----------------

.. automodule:: kr.config
   :members:
   :undoc-members:
   :show-inheritance:

kr.server module
----------------

.. automodule:: kr.server
   :members:
   :undoc-members:
   :show-inheritance:

kr.work module
--------------

.. automodule:: kr.work
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: kr
   :members:
   :undoc-members:
   :show-inheritance:
