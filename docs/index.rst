.. KR documentation master file, created by
   sphinx-quickstart on Wed Dec 11 11:41:31 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KR's documentation!
==============================

.. .. automodule:: kr
..     :members:
..     :undoc_members:

.. toctree:: kr
   :maxdepth: 6
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
