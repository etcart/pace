var path = require('path');
var webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractSass = new ExtractTextPlugin({
    filename: '../css/kr.css'
});

// console.log(__dirname);

module.exports = {
    mode: 'development',
    entry: './web/js/web.js',
    output: {
        path: path.resolve(__dirname, 'web/js'),
        filename: 'web.bundle.js'
    },
    module: {
        rules: [{
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: ['css-loader', 'sass-loader']
                }),
            },
        ]
    },
    plugins: [
        extractSass
    ],
    stats: {
        colors: true
    },
    devtool: 'source-map'
};